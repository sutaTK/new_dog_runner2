﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{
    public Controller con;
    public PathGenerator pg;

    

    public bool paused = false;
    // run Panels
    public GameObject losePanel;
    public GameObject runBar;
    public GameObject runPause;
    public GameObject PausePanel;
    public GameObject gameUI;
    public GameObject runScore;
    // menu Panels
    public GameObject settingsPanel;
    public GameObject shopPanel;



    void Start()
    {
        
        Time.timeScale = 1;
        runBar.SetActive(false);
        losePanel.SetActive(false);
        runPause.SetActive(false);
        PausePanel.SetActive(false);
        settingsPanel.SetActive(false);
        shopPanel.SetActive(false);
        runScore.SetActive(false);
    }

    public void SettingsMenu()
    {
        settingsPanel.SetActive(true);
    }
    public void ExitSettings()
    {
        settingsPanel.SetActive(false);
    }

    public void ShopMenu()
    {
        shopPanel.SetActive(true);
    }
    public void ExitShop()
    {
        shopPanel.SetActive(false);
    }


    public void gameState()
    {
        if(con.gameLose == true)
        {
            StartCoroutine(GameLose());
        }
        else if(con.gameLose == false)
        {
            losePanel.SetActive(false);
            
        }
    }

    IEnumerator GameLose()
    {
        runBar.SetActive(false);
        runPause.SetActive(false);
        runScore.SetActive(false);
        yield return new WaitForSeconds(0.6f);
        losePanel.SetActive(true);
        Time.timeScale = 0;
    }
   
    void Update()
    {
        if (con.RunStarted)
        {
            runScore.SetActive(true);
            runBar.SetActive(true);
            runPause.SetActive(true);
            gameUI.SetActive(false);

        }
    
        gameState();

        if(pg.finished == true)
        {
            paused = true;
            runBar.SetActive(false);
            runPause.SetActive(false);
            runScore.SetActive(false);
            Time.timeScale = 0;
        }
    }
    public void Pasuse()
    {
        paused = true;
        PausePanel.SetActive(true);
        Time.timeScale = 0;
        
    }

   

    public void Resume()
    {
        paused = false;
        PausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
