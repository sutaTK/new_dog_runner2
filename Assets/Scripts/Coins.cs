﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coins : MonoBehaviour
{
    public int coins;
    int allCoins;
    public Text runCoinText;
    public Text pauseCoinText;
    public Text totalCoinText;
    public Text allCoinText;
    public Text winCoin;
    string allCoinPref = "prefCoin";

    public Controller con;

    void Start()
    {
        coins = PlayerPrefs.GetInt(allCoinPref);
    }


    void Update()
    {
        runCoinText.text = "" + PlayerPrefs.GetInt("coins");
        pauseCoinText.text = "" + PlayerPrefs.GetInt("coins"); 
        totalCoinText.text = "" + PlayerPrefs.GetInt("coins"); 
        winCoin.text = "" + PlayerPrefs.GetInt("coins");

        if (con.gameLose == true)
        {
            allCoins = PlayerPrefs.GetInt("coins"); ;
            PlayerPrefs.SetInt(allCoinPref, allCoins);
            PlayerPrefs.Save();
        }

        allCoinText.text = "" + PlayerPrefs.GetInt(allCoinPref);
        //Debug.Log(coins);
    }


}
