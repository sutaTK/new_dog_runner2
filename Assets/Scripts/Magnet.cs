﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    public GameObject coinDetect;

    void Start()
    {
        //префаб магнита должен быть в сцене
        coinDetect = GameObject.FindGameObjectWithTag("CoinDetector");
        coinDetect.SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(ActivateCoin());
            Destroy(transform.GetChild(0).gameObject);
        }
    }
    IEnumerator ActivateCoin()
    {
        coinDetect.SetActive(true);
        yield return new WaitForSeconds(10f);
        coinDetect.SetActive(false);
    }
}
